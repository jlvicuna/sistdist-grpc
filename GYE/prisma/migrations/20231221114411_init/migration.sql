-- CreateTable
CREATE TABLE "facture" (
    "id" SERIAL NOT NULL,
    "ciudad" TEXT NOT NULL,
    "empleado" TEXT NOT NULL,
    "cliente" TEXT NOT NULL,
    "codigo" TEXT NOT NULL,
    "fecha" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "valor_total" DOUBLE PRECISION NOT NULL,
    "valor_subTotal" DOUBLE PRECISION NOT NULL,
    "valor_iva" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "facture_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "details_facture" (
    "id" SERIAL NOT NULL,
    "factura_id" INTEGER NOT NULL,
    "cantidad" INTEGER NOT NULL,

    CONSTRAINT "details_facture_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "product" (
    "id" SERIAL NOT NULL,
    "detalle_id" INTEGER NOT NULL,
    "cantidad" INTEGER NOT NULL,
    "nombre" TEXT NOT NULL,
    "valor_unitario" DOUBLE PRECISION NOT NULL,
    "valorpor_producto" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "product_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "details_facture" ADD CONSTRAINT "details_facture_factura_id_fkey" FOREIGN KEY ("factura_id") REFERENCES "facture"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "product" ADD CONSTRAINT "product_detalle_id_fkey" FOREIGN KEY ("detalle_id") REFERENCES "details_facture"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
