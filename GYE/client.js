import grpcLib from '@grpc/grpc-js';
import protoLoaderLib from '@grpc/proto-loader';

// Define the path to the ProtoBuf model file
const PROTO_PATH = './modelo_factura.proto';

// Load the protobuf file with specific options to retain field casing and convert certain types to strings
const protoOptions = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
};
const packageDef = protoLoaderLib.loadSync(PROTO_PATH, protoOptions);

// Generate a service object from the loaded protobuf definition
const factureServicePackage = grpcLib.loadPackageDefinition(packageDef);

// Instantiate a client for the Facture service
const facturaServiceClient = new factureServicePackage.FactureService(
  'localhost:7373', 
  grpcLib.credentials.createInsecure() // Use insecure credentials for the example
);

// Make the client available for import elsewhere in the application
export default facturaServiceClient;