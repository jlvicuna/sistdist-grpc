import express from "express";
import grpcClient from '../client.js';
import logger from "morgan";

import { createInvoice, fetchAllInvoices } from "./invoiceQueries.js";

const server = express()

server.use(express.json())
server.use(logger('dev'))
server.use(express.urlencoded({ extended: false }))

server.get('/central', (req, response) => {
    grpcClient.retrieveInvoice(null, async (err, invoiceData) => {
        if (!err) {
            console.log(invoiceData)
            response.send(invoiceData)
        } else {
            response.status(500).send({ message: err })
        }
    })
})

server.get('/', async (req, response) => {
    const invoices = await fetchAllInvoices()
    response.send(invoices)
})

server.post('/new', (req, response) => {
    const newInvoice = req.body;
    if (newInvoice) {
        grpcClient.createInvoice(newInvoice, async (err, invoiceResponse) => {
            if (!err) {
                await createInvoice(newInvoice);
                response.send({ message: 'Invoice successfully added.' });
            } else {
                response.status(500).send({ message: err });
            }
        });
    } else {
        response.status(400).send({ message: 'Invoice data not provided in the request.' });
    }
});


server.listen(8080, () => {
    console.log('Invoice Server running on port 8080.')
})
