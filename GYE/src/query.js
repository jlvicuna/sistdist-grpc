import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function createFactura(invoiceData) {
    const { ciudad, empleado, cliente_nombre, codigo, fecha, valor_total, valor_subTotal, valor_iva, detalle } = invoiceData;

    const newInvoice = await prisma.invoice.create({
        data: {
            ciudad, empleado, cliente_nombre, codigo, fecha, valor_total, valor_subTotal, valor_iva
        },
    });

    const detalles_factura = await prisma.invoiceDetails.create({
        data: {
            factura_id: factura.id,
            cantidad: detalle.cantidad
        },
    });

    const productEntries = details.products.map(item => {
        return prisma.product.create({
            data: {
                detalle_id: detalles_factura.id,

                cantidad: producto.cantidad,

                nombre: producto.nombre,

                valor_unitario: producto.valor_unitario,

                valor_total: producto.valor_total
            },
        });
    });

    await Promise.all(productEntries);

    return newInvoice;
}

async function fetchAllFacturas() {
    const facturas = await prisma.invoice.findMany()
    return facturas
}

export { createFactura, fetchAllFacturas }
