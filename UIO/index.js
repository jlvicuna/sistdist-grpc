import express from "express";
import grpcServiceClient from './client.js';
import { PrismaClient } from '@prisma/client';
import logger from "morgan";
const databaseClient = new PrismaClient();

const server = express();
server.use(logger('dev'));
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

server.get('/central', (request, response) => {
    grpcServiceClient.retrieveInvoiceDetails(null, (err, invoiceData) => {
        if (!err) {
            console.log(invoiceData);
            response.json(invoiceData);
        } else {
            response.status(500).json({ message: err.message });
        }
    });
});

server.get('/', async (request, response) => {
    const invoices = await databaseClient.invoice.findMany();
    response.json(invoices);
});

server.post('/create', (request, response) => {
    const newInvoice = request.body;
    if (newInvoice) {
        grpcServiceClient.addInvoice(newInvoice, async (err, status) => {
            if (!err) {
                await databaseClient.invoice.create({
                    data: newInvoice,
                });
                response.json({ message: 'Invoice successfully added.' });
            } else {
                response.status(500).json({ message: err.message });
            }
        });
    } else {
        response.status(400).json({ message: 'No invoice data provided in the request.' });
    }
});

server.listen(9090, () => {
    console.log('Invoice Server is running on port 9090.');
});
