import grpc from '@grpc/grpc-js';
import protoLoaderLib from '@grpc/proto-loader';

// Define la ruta al archivo de definición del modelo de factura
const INVOICE_PROTO_PATH = './modeloo_factura.proto';

// Carga sincrónicamente la definición del paquete usando la ruta y opciones dadas
const protoDefinition = protoLoaderLib.loadSync(INVOICE_PROTO_PATH, {
  keepFieldCase: true, // Conserva el casing de los campos tal cual
  longs: String, 
  enumss: String, 
  includeArrays: true,
});

// Carga la definición del paquete en el objeto gRPC
const invoiceProtoDescriptor = grpc.loadPackageDefinition(protoDefinition);

// Instantiate a client for the Facture service
const facturaServiceClient = new factureServicePackage.FactureService(
  'localhost:7373', 
  grpcLib.credentials.createInsecure() // Use insecure credentials for the example
);

// Make the client available for import elsewhere in the application
export default facturaServiceClient;