import grpcLib from '@grpc/grpc-js';
import protoLoaderLib from '@grpc/proto-loader';

// Define la ubicación del archivo de definición de protocolo para las facturas
const INVOICE_PROTO_DEFINITION = './modelo_factura.proto';

// Carga las definiciones del archivo .proto con opciones específicas
const protoConfig = protoLoaderLib.loadSync(INVOICE_PROTO_DEFINITION, {
  keepFieldNames: true,
  longs:         String,
  enums:         String,
  includeArrays: true,
});

// Crea un objeto de protocolo a partir de las definiciones cargadas
const invoiceProtocol = grpcLib.loadPackageDefinition(protoConfig);

// Inicializa el cliente de servicios de factura usando el protocolo creado
const invoiceServiceClient = new invoiceProtocol.InvoiceService(
  'localhost:7373', // Especifica el host y puerto del servicio
  grpcLib.credentials.createInsecure() // Utiliza credenciales no seguras para la conexión
);

//
