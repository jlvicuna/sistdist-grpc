import grpc from '@grpc/grpc-js'
import protoLoader from '@grpc/proto-loader'
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const FACTURE_MODEL = './facture_mdl.proto';


async function main() {
    const packageDefinition = protoLoader.loadSync(FACTURE_MODEL, {
        keepCase: true,
        longs: String,
        enums: String,
        arrays: true,
    })

    const factureProto = grpc.loadPackageDefinition(packageDefinition)
    const server = new grpc.Server()



    server.addService(factureProto.FactureService.service, {
        getFacture: async (_, callback) => {
            const factures = await prisma.Facture.findMany()
            callback(null, {"factures":factures});
        },
        addFacture: async (call, callback) => {
            const facture = call.request
            await prisma.Facture.create({
                data: facture,
            });
            console.log(facture)
            callback(null, {"msg": true})
        }
    })


    server.bindAsync(
        '127.0.0.1:7373',
        grpc.ServerCredentials.createInsecure(),
        () => {
            server.start()
            console.log('Server GYE/UIO RPC on port 7373.')
        }
    )
}

main()